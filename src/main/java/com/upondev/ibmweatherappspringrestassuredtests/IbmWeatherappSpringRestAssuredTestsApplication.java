package com.upondev.ibmweatherappspringrestassuredtests;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IbmWeatherappSpringRestAssuredTestsApplication {

	public static void main(String[] args) {
		SpringApplication.run(IbmWeatherappSpringRestAssuredTestsApplication.class, args);
	}

}
