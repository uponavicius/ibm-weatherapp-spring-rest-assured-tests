package com.upondev.ibmweatherappspringrestassuredtests;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;

public class IbmWeatherApiRestAssuredEndpointTests {

    String applicationJson = "application/json";

    @BeforeEach
    void setUp() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 8080;

    }

    @Test
    void test_GetVilniusWeatherFromDB() {
        String date = LocalDateTime.now().toString().substring(0, 10);

        Response response = given()
                .accept(applicationJson)
                .queryParam("date", date)
                .when()
                .get("/weather")
                .then()
                .statusCode(200)
                .contentType(applicationJson)
                .extract()
                .response();

        assertNotNull(response);

        List<Object> objects = response.jsonPath().getList("");
        Map<String, Object> object = (Map<String, Object>) objects.get(0);

        String tempValue = object.get("tempValue").toString();
        assertNotNull(tempValue);

        String observationTimeValue = object.get("observationTimeValue").toString();
        assertNotNull(observationTimeValue);

        String id = object.get("id").toString();
        assertNotNull(id);

        String tempUnits = object.get("tempUnits").toString();
        assertEquals(tempUnits, "C");

    }

    @Test
    void test_GetMinDate() {
        Response response = given()
                .accept(applicationJson)
                .when()
                .get("/minDate")
                .then()
                .statusCode(200)
                .contentType(applicationJson)
                .extract()
                .response();

        assertNotNull(response);

        // This test works if the database is started filling today
        String dateFromDb = response.getBody().asString().substring(2, 12);
        String dateToCheck = LocalDateTime.now().minusMonths(1).toString().substring(0, 10);
        assertEquals(dateFromDb, dateToCheck);


    }
}
