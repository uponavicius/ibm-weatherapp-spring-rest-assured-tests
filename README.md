# ibm-weatherapp-spring-rest-assured-tests

IBM WeatherApp Spring Rest Assured Tests

With this project can test `ibm-weatherapp-spring` project https://gitlab.com/uponavicius/ibm-weatherapp-spring

### To start project:

* Clone repository https://gitlab.com/uponavicius/ibm-weatherapp-spring-rest-assured-tests.git
* Open with your favorite IDEA (example IntelliJ IDEA)
* ATTENTION! First must run `ibm-weatherapp-spring` project. 
* In `ibm-weatherapp-spring-rest-assured-tests` project go to `src\test\java\com\upondev\ibmweatherappspringrestassuredtests` and run this class `IbmWeatherApiRestAssuredEndpointTests`

